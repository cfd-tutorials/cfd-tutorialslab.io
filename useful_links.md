# A collection of useful links for setting up GitBook

## GitBook document structure and style

- GitBook Toolchain Documentation: [link](https://gitbookio.gitbooks.io/docs-toolchain/)

## Math integration

- Mathjax plugin repository: [link](https://github.com/GitbookIO/plugin-mathjax)
- *load plugin mathjax* issue and fix: [link](https://github.com/GitbookIO/plugin-mathjax/issues/13)
